import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class JsonLeser {

    private DatabaseHandterer database;
    private URL url;
    private HttpURLConnection request;

    public JsonLeser() {
        database = new DatabaseHandterer();
    }

    // TRAVERSERING:

    // Går gjennom fra angitt startsesong til sluttsesong og alle deres divisjoner og grupper. Skriver ut hvilken sesong det arbeides med.
    public void hentResultater(int start, int slutt) throws Exception {
        for (int i = start; i <= slutt; i++) {
            System.out.print("Sesong " + i + ":...");
            traverserDivisjoner(i);
        }
    }

    // Traverserer alle kamper i siste sesong og lagrer dem med hjelpemetoden finnOgLagreKamper(). Skriver ut hvilken spiller det arbeides med.
    public void hentKamper(int sesong) throws Exception {
        ArrayList<String> navn = hentNavnPrGruppe(sesong);
        int teller = 1;
        for (String spiller : navn) {

            JsonArray array = finnArray("http://api.aasmul.net/matches/" + spiller + "/1");
            finnOgLagreKamper(array, sesong);
            System.out.println("Ferdig med spillernr " +teller+ ", " +spiller+ ", av " +navn.size());
            teller++;
        }
    }

    public void tilkoble() throws Exception {
        database.tilkoble();
    }

    public void frakoble() {
        database.frakoble();
    }

    // HJELPEMETODER:

    // Hjelpemetode til hentResultater(). Går gjennom divisjonene i en gitt sesong. Skriver ut prosentvis fremgang.
    private void traverserDivisjoner(int sesong) throws Exception{
        int antDiv = finnArray("http://api.aasmul.net/divisions/" + sesong + "/1").size();
        int deler = 100/antDiv;
        int sum = 0;

        for (int i = 1; i <= antDiv; i++) {
            traverserGrupper(sesong, i);
            sum += deler;
            if (i == antDiv) {
                System.out.print("100%\n");
            } else {
                System.out.print(sum + "%...");
            }
        }
    }

    // Hjelpemetode til traverserDivisjoner(). Går gjennom gruppen i en gitt divisjon og lagrer personer og resultater.
    private void traverserGrupper(int sesong, int divisjon) throws Exception{
        int antGrupper = finnArray("http://api.aasmul.net/groups/" + divisjon + "/" + sesong + "/1").size();

        for (int i = 1; i <= antGrupper; i++) {
            JsonArray array = finnArray("http://api.aasmul.net/table/1/" + sesong + "/" + divisjon + "/" + i);
            finnOgLagrePersoner(array);
            finnOgLagreResultater(array, sesong, divisjon, i);
        }
    }

    // Hjelpemetode til hentKamper(). Returnerer en arraylist med et spillernavn pr gruppe.
    private ArrayList<String> hentNavnPrGruppe(int sesong) throws Exception{
        ArrayList<String> navn = new ArrayList<String>();

        // Går gjennom divisjonene
        int andDiv = finnArray("http://api.aasmul.net/divisions/" + sesong + "/1").size();
        for (int i = 1; i <= andDiv; i++) {

            // Går gjennom gruppene
            int antGruppe = finnArray("http://api.aasmul.net/groups/" + i + "/" + sesong + "/1").size();
            for (int g = 1; g <= antGruppe; g++) {
                JsonArray array = finnArray("http://api.aasmul.net/table/1/" + sesong + "/" + i + "/" + g);

                // Unngår å hente ut et spillernavn som innholder spesielle tegn, da dette vil skape problemer med url
                int indeks = 0;
                String spiller = array.get(indeks).getAsJsonObject().get("Navn").getAsString();
                while (!statusOk(spiller)) {
                    indeks++;
                    spiller = array.get(indeks).getAsJsonObject().get("Navn").getAsString();
                }

                navn.add(spiller);
            }
            System.out.println("Hentet navn fra divisjon " +i);
        }
        return navn;
    }

    // Hjelpemetode til hentNavnPrGruppe(). Sjekker om man får OK status på oppslag på spiller
    private boolean statusOk(String spiller) throws Exception{
        url = new URL("http://api.aasmul.net/matches/" + spiller + "/1");
        request = (HttpURLConnection)url.openConnection();
        return (request.getResponseCode() == 200);
    }

    // Hjelpemetode som returnerer en JSON array på gitt url
    private JsonArray finnArray(String sURL) throws IOException{

        // Kobler til URL
        url = new URL(sURL);
        request = (HttpURLConnection)url.openConnection();

        // Konverterer til et JSON element
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream)request.getContent()));

        // Henter og returnerer elementet som en JSON array
        return root.getAsJsonArray();
    }

    // Hjelpemetode som henter ut personinfo fra en JsonArray og lagrer det via lagrePersoner() fra DatabaseHandterer
    private void finnOgLagrePersoner(JsonArray array) throws Exception{
        for (int i = 0; i < array.size(); i++) {
            String navn = array.get(i).getAsJsonObject().get("Navn").getAsString();
            String url = array.get(i).getAsJsonObject().get("Url").getAsString();
            String sted = array.get(i).getAsJsonObject().get("Fra").getAsString();
            String født = array.get(i).getAsJsonObject().get("Alder").getAsString();
            int rank = 0;

            database.lagrePersoner(navn, url, sted, født, rank);
        }
    }

    // Hjelpemetode som henter ut resultatinfo fra en JsonArray og lagrer det via lagreResultat() fra DatabaseHandterer
    private void finnOgLagreResultater(JsonArray array, int sesong, int divisjon, int gruppe) throws Exception{
        for (int i = 0; i < array.size(); i++) {
            int plassering = array.get(i).getAsJsonObject().get("Plass").getAsInt();
            int antSpillere = array.size();
            int antSpilt = array.get(i).getAsJsonObject().get("Spilt").getAsInt();
            int vunnet = array.get(i).getAsJsonObject().get("Seier").getAsInt();
            int uavgjort = array.get(i).getAsJsonObject().get("Uavgjort").getAsInt();
            int tapt = array.get(i).getAsJsonObject().get("Tap").getAsInt();
            String[] poengTab = array.get(i).getAsJsonObject().get("For-mot").getAsString().split(" ");
            int poengFor = Integer.parseInt(poengTab[0]);
            int poengMot = Integer.parseInt(poengTab[2]);
            int kamppoeng = array.get(i).getAsJsonObject().get("Poeng").getAsInt();
            String person_navn = array.get(i).getAsJsonObject().get("Navn").getAsString();

            database.lagreResultat(plassering, antSpillere, sesong, divisjon, gruppe, antSpilt, vunnet, uavgjort, tapt, poengFor, poengMot, kamppoeng, person_navn);
        }
    }

    //Hjelpemetode som henter ut kampinfo fra en JsonArray og lagrer dem via lagreKamp() fra DatabaseHandterer
    private void finnOgLagreKamper(JsonArray array, int sesong) throws Exception{
        for(int i = 0; i < array.size(); i++) {
            try {
                String resultat = array.get(i).getAsJsonObject().get("result").getAsString();
                if(!resultat.equalsIgnoreCase("")) {
                    String[] spillere = array.get(i).getAsJsonObject().get("match").getAsString().split(" - ");
                    String navn1 = spillere[0];
                    String navn2 = spillere[1];
                    String[] poengTab = array.get(i).getAsJsonObject().get("result").getAsString().split(" - ");
                    int poeng_navn1 = Integer.parseInt(poengTab[0]);
                    int poeng_navn2 = Integer.parseInt(poengTab[1]);

                    database.lagreKamp(sesong, navn1, navn2, poeng_navn1, poeng_navn2);
                }
            } catch (UnsupportedOperationException e) {
                // Hit kommer vi hvis Json objektet ikke er en person men en tittel. Går da videre til neste objekt.
            }
        }
    }
}
