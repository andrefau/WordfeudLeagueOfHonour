import com.mongodb.*;

public class DatabaseHandterer {

    private MongoClient mongoKlient;
    private DB database;
    private DBCollection personColl;
    private DBCollection resultatColl;
    private DBCollection kampColl;
    private DBCollection statColl;
    private DBCollection divStatColl;
    private DBCollection testColl;

    // LAGRING:

    // Lagrer et nytt resultatdokument i databasen. Kalles fra klassen JsonLeser.
    public void lagreResultat(int plassering, int antSpillere, int sesong, int divisjon, int gruppe, int antSpilt, int vunnet, int uavgjort,
                                     int tapt, int poengFor, int poengMot, int kamppoeng, String personNavn) throws Exception {

        DBObject resultat = new BasicDBObject("plassering", plassering)
                .append("antall spillere", antSpillere)
                .append("sesong", sesong)
                .append("divisjon", divisjon)
                .append("gruppe", gruppe)
                .append("antall spilt", antSpilt)
                .append("vunnet", vunnet)
                .append("uavgjort", uavgjort)
                .append("tapt", tapt)
                .append("poeng for", poengFor)
                .append("poeng mot", poengMot)
                .append("kamppoeng", kamppoeng)
                .append("person_navn", personNavn);
        resultatColl.insert(resultat);
    }

    // Lagrer et nytt persondokument i databasen. Kalles fra klassen JsonLeser. Hvis personen finnes oppdateres den.
    public void lagrePersoner(String navn, String url, String sted, String født, int rank) throws Exception {

        if (!personFinnes(url)) {

            DBObject person = new BasicDBObject("_id", url)
                    .append("Navn", navn)
                    .append("Sted", sted)
                    .append("Født", født)
                    .append("Rank", rank);
            personColl.insert(person);
        } else {
            DBObject person = new BasicDBObject("_id", url);
            person.put("Navn", navn);
            person.put("Sted", sted);
            person.put("Født", født);
            person.put("Rank", rank);
            personColl.update(new BasicDBObject("_id", url), person);
        }
    }

    // Oppdaterer personcollection med et felt for totalt antall spilte sesonger
    public void oppdaterPerson(DBObject person) throws Exception {
        personColl.update(new BasicDBObject("_id", person.get("_id")), person);
    }

    // Lagrer et nytt kampdokument i databasen
    public void lagreKamp(int sesong, String navn1, String navn2, int poengNavn1, int poengNavn2) throws Exception {

        if (!kampFinnes(sesong, navn1, navn2, poengNavn1, poengNavn2)) {
            DBObject resultat = new BasicDBObject("sesong", sesong)
                    .append("person_navn1", navn1)
                    .append("person_navn2", navn2)
                    .append("poeng_person1", poengNavn1)
                    .append("poeng_person2", poengNavn2);
            kampColl.insert(resultat);
        }
    }

    // Lagrer et nytt statistikkdokument i databasen. Inneholder generell statistikk for hver sesong
    public void lagreStatistikk(int sesong, int antDivisjon, int antGrupper, int antSpillere, int antUavgjort, int poeng) throws Exception {
        DBObject stat = new BasicDBObject("sesong", sesong)
                .append("antall_divisjoner", antDivisjon)
                .append("antall_grupper", antGrupper)
                .append("antall_spillere", antSpillere)
                .append("antall_uavgjort", antUavgjort)
                .append("antall_wfpoeng", poeng);
        statColl.insert(stat);
    }

    // Lagrer et nytt divisjon_statistikk dokument i databasen. Inneholder info for hver divisjon
    /*public void lagreDivisjonStatistikk(int sesong, int divisjon, int antGrupper, int antSpillere, int poeng, int antUavgjort) throws Exception {
        DBObject stat = new BasicDBObject("sesong", sesong)
                .append("divisjon", divisjon)
                .append("antall_grupper", antGrupper)
                .append("antall_spillere", antSpillere)
                .append("antall_wfpoeng", poeng)
                .append("antall_uavgjort", antUavgjort);
        divStatColl.insert(stat);
    }*/

    public void lagreDivisjonStatistikk(DBObject stat) throws Exception {
        divStatColl.insert(stat);
    }

    // DATABASEKOMMUNIKASJON

    // Oppretter forbindelsen til databasen
    public void tilkoble() throws Exception {
        mongoKlient = new MongoClient();
        database = mongoKlient.getDB("WLOH");
        personColl = database.getCollection("personer");
        resultatColl = database.getCollection("resultater");
        kampColl = database.getCollection("kamper");
        statColl = database.getCollection("statistikk");
        divStatColl = database.getCollection("divisjon_statistikk");
        testColl = database.getCollection("test");
    }

    public void frakoble() {
        mongoKlient.close();
    }

    public DBCollection hentPersonColl() {
        return  personColl;
    }

    public DBCollection hentResultatColl() {
        return resultatColl;
    }

    public DBCollection hentKampColl() {
        return kampColl;
    }

    public DBCollection hentStatColl() { return statColl; }

    public DBCollection hentDivStatColl() { return divStatColl; }

    public DBCollection hentTestColl() { return testColl; }

    // HJELPEMETODER:

    // Hjelpemetode til lagrePersoner(). Sjekker om en person finnes eller ikke.
    private boolean personFinnes(String id) {
        try {
            DBObject query = new BasicDBObject("_id", id);
            DBCursor cursor = personColl.find(query);
            cursor.one().get("Navn");
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    // Hjelpemetode til lagreKamp(). Sjekker om en kamp finnes eller ikke.
    private boolean kampFinnes(int sesong, String navn1, String navn2, int poeng_navn1, int poeng_navn2) {
        try {
            DBObject query = new BasicDBObject();
            query.put("sesong", sesong);
            query.put("person_navn1", navn1);
            query.put("person_navn2", navn2);
            query.put("poeng_person1", poeng_navn1);
            query.put("poeng_person2", poeng_navn2);
            DBCursor cursor = kampColl.find(query);
            cursor.one().get("person_navn1");
            return true;
        } catch (NullPointerException e) {
            try {
                DBObject query = new BasicDBObject();
                query.put("sesong", sesong);
                query.put("person_navn1", navn2);
                query.put("person_navn2", navn1);
                query.put("poeng_person1", poeng_navn2);
                query.put("poeng_person2", poeng_navn1);
                DBCursor cursor = kampColl.find(query);
                cursor.one().get("person_navn1");
                return true;
            } catch (NullPointerException i) {
                return false;
            }
        }

    }
}
