import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mongodb.*;
import com.mongodb.util.JSON;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

public class Statistikk {

    private DatabaseHandterer dbHandterer;

    public DatabaseHandterer getDbHandterer() {
        return dbHandterer;
    }

    public Statistikk() {
        dbHandterer = new DatabaseHandterer();
    }

    // Metode for å koble til databasen
    public boolean tilkoble() {
        try {
            dbHandterer.tilkoble();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    // Metode for å koble fra databasen
    public boolean frakoble() {
        try {
            dbHandterer.frakoble();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    // Finner antall spillere i en bestemt sesong
    public int finnAntallSpillereSesong(int sesong) {
        DBObject query = new BasicDBObject();
        query.put("sesong", sesong);
        DBCursor cursor = dbHandterer.hentResultatColl().find(query);
        return cursor.size();
    }

    // Finner antall spillere i alle spilte sesonger
    public ArrayList<Integer> finnAntallSpillerePerSesong() {
        ArrayList<Integer> spillerPerSesong = new ArrayList<Integer>();
        int sisteSesong = sisteSesong();
        for(int i = 1; i <= sisteSesong; i++) {
            spillerPerSesong.add(finnAntallSpillereSesong(i));
        }
        return spillerPerSesong;
    }

    public ArrayList<Integer> finnAntallUavgjortPerSesong() {
        ArrayList<Integer> uavgjortPerSesong = new ArrayList<Integer>();
        int sisteSesong = sisteSesong();
        for(int i = 1; i <= sisteSesong; i++) {
            uavgjortPerSesong.add(finnAntallUavgjortSesong(i));
        }
        return uavgjortPerSesong;
    }

    public int finnAntallUavgjortSesong(int sesong) {
        DBObject query = new BasicDBObject();
        query.put("sesong", sesong);
        DBCursor cursor = dbHandterer.hentResultatColl().find(query);
        int antallUavgjort = 0;
        while(cursor.hasNext()) {
            DBObject o = cursor.next();
            antallUavgjort += Integer.parseInt(o.get("uavgjort").toString());
        }
        return antallUavgjort/2; //Siden alle uavgjort-resultater dukker opp dobbelt
    }

    public double finnAntallUavgjortDivisjon(int sesong, int divisjon) {
        DBObject query = new BasicDBObject("sesong", sesong).append("divisjon", divisjon);
        DBCursor cursor = dbHandterer.hentResultatColl().find(query);
        double antallUavgjort = 0;
        while(cursor.hasNext()) {
            DBObject o = cursor.next();
            antallUavgjort += Integer.parseInt(o.get("uavgjort").toString());
        }
        return antallUavgjort/2; //Siden alle uavgjort-resultater dukker opp dobbelt
    }

    public double finnAntallUavgjortGruppe(int sesong, int divisjon, int gruppe) {
        DBObject query = new BasicDBObject("sesong", sesong).append("divisjon", divisjon).append("gruppe", gruppe);
        DBCursor cursor = dbHandterer.hentResultatColl().find(query);
        double antallUavgjort = 0;
        while(cursor.hasNext()) {
            DBObject o = cursor.next();
            antallUavgjort += Integer.parseInt(o.get("uavgjort").toString());
        }
        return antallUavgjort/2; //Siden alle uavgjort-resultater dukker opp dobbelt
    }

    public void lagreGenerellStatistikk() throws Exception{
        for(int i = 1; i <= sisteSesong(); i++) {
            int antSpillere = finnAntallSpillereSesong(i);
            int antUavgjort = finnAntallUavgjortSesong(i);
            int antPoeng = finnSesongInfo(i, "$poeng for");
            int divisjon = sisteDivisjon(i);
            int sumGruppe = 0;
            for (int k = 1; k <= divisjon; k++) {
                sumGruppe += sisteGruppe(i, k);
            }
            dbHandterer.lagreStatistikk(i, divisjon, sumGruppe, antSpillere, antUavgjort, antPoeng);
            System.out.println("Ferdig sesong: " +i);
        }
    }

    /*public void lagreDivisjonStatistikk() throws Exception{
        for(int s = 1; s <= sisteSesong(); s++) {
            int sisteDivisjon = sisteDivisjon(s);

            for(int d = 1; d <= sisteDivisjon; d++) {
                int antGrupper = sisteGruppe(s, d);
                int antSpillere = 0;

                for(int g = 1; g <= antGrupper; g++) {
                    DBCursor cursor = getDbHandterer().hentResultatColl().find(new BasicDBObject("sesong", s)
                                                                         .append("divisjon", d)
                                                                         .append("gruppe", g));
                    antSpillere += Integer.parseInt(cursor.one().get("antall spillere").toString());
                }

                int antWFPoeng = finnDivisjonInfo(s, d, "$poeng for");
                Double verdi = finnAntallUavgjortDivisjon(s, d);
                int antUavgjort = verdi.intValue();

                dbHandterer.lagreDivisjonStatistikk(s, d, antGrupper, antSpillere, antWFPoeng, antUavgjort);
            }

            System.out.println("Ferdig sesong: " +s);
        }
    }*/

    public void lagreDivisjonStatistikk() throws Exception{
        for(int s = 1; s <= sisteSesong(); s++) {
            int sisteDivisjon = sisteDivisjon(s);

            for(int d = 1; d <= sisteDivisjon; d++) {
                DBObject divStat = new BasicDBObject();
                BasicDBList grupper = new BasicDBList();

                int antGrupper = sisteGruppe(s, d);
                int antSpillere = 0;

                for(int g = 1; g <= antGrupper; g++) {
                    DBCursor cursor = getDbHandterer().hentResultatColl().find(new BasicDBObject("sesong", s)
                            .append("divisjon", d)
                            .append("gruppe", g));
                    int antSpillereIGruppe = Integer.parseInt(cursor.one().get("antall spillere").toString());
                    antSpillere += antSpillereIGruppe;

                    Double antUavgjortGruppe = finnAntallUavgjortGruppe(s, d, g);
                    int ant = antUavgjortGruppe.intValue();
                    DBObject gruppeStat = new BasicDBObject("gruppe", g)
                                            .append("antall_spillere", antSpillereIGruppe)
                                            .append("antall_wfpoeng", finnGruppeInfo(s, d, g, "$poeng for"))
                                            .append("antall_uavgjort", ant);

                    grupper.add(gruppeStat);
                }

                int antWFPoeng = finnDivisjonInfo(s, d, "$poeng for");
                Double verdi = finnAntallUavgjortDivisjon(s, d);
                int antUavgjort = verdi.intValue();

                divStat.put("sesong", s);
                divStat.put("divisjon", d);
                divStat.put("antall_grupper", antGrupper);
                divStat.put("antall_spillere", antSpillere);
                divStat.put("antall_wfpoeng", antWFPoeng);
                divStat.put("antall_uavgjort", antUavgjort);
                divStat.put("grupper", grupper);
                dbHandterer.lagreDivisjonStatistikk(divStat);
            }

            System.out.println("Ferdig sesong: " +s);
        }
    }

    // Metoder for å validere input
    public boolean validerInput(String spiller) {
        return spillerFinnes(spiller);
    }

    public boolean validerInput(String spiller, int sesong) {
        return spillerFinnes(spiller) && sesong > 0 && sesong <= sisteSesong() && sisteSesong() != -1;
    }

    public boolean validerInput(String spiller1, String spiller2, int sesong) {
        return spillerFinnes(spiller1) && spillerFinnes(spiller2) && sesong > 0 && sesong <= sisteSesong() && sisteSesong() != -1;
    }

    public boolean spillerErISesong(String spiller, int sesong) {
        return dbHandterer.hentResultatColl().find(new BasicDBObject("person_navn", spiller).append("sesong", sesong)).one() != null;
    }

    // Hent spiller
    public DBObject finnSpillerInfo(String spiller) {
        return dbHandterer.hentPersonColl().find(new BasicDBObject("Navn", spiller)).one();
    }

    // Sannsynligheten for at en bestemt spiller vil vinne mot en annen spiller
    public double sannsSpillerVilVinne(String spiller1, String spiller2, int sesong) throws Exception {
        DBObject query = new BasicDBObject();
        query.put("person_navn1", spiller1);
        query.put("person_navn2", spiller2);
        DBCursor cursor = dbHandterer.hentKampColl().find(query);
        int vunnetSpiller1 = 0;
        int vunnetSpiller2 = 0;
        int spilt = cursor.size();

        while (cursor.hasNext()) {
            DBObject o = cursor.next();
            if (Integer.parseInt(o.get("poeng_person1").toString()) > Integer.parseInt(o.get("poeng_person2").toString())) {
                vunnetSpiller1++;
            } else {
                vunnetSpiller2++;
            }
        }

        query = new BasicDBObject();
        query.put("person_navn1", spiller2);
        query.put("person_navn2", spiller1);
        cursor = dbHandterer.hentKampColl().find(query);
        spilt += cursor.size();

        while (cursor.hasNext()) {
            DBObject o = cursor.next();
            if (Integer.parseInt(o.get("poeng_person2").toString()) > Integer.parseInt(o.get("poeng_person1").toString())) {
                vunnetSpiller1++;
            } else {
                vunnetSpiller2++;
            }
        }

        double forrigeSpiltSpiller1 = finnSpillerInfoForrigeSesong(spiller1, "$antall spilt", sesong);
        double forrigeSpiltSpiller2 = finnSpillerInfoForrigeSesong(spiller2, "$antall spilt", sesong);
        double forrigeVunnetSpiller1 = finnSpillerInfoForrigeSesong(spiller1, "$vunnet", sesong);
        double forrigeVunnetSpiller2 = finnSpillerInfoForrigeSesong(spiller2, "$vunnet", sesong);

        double totalSpiltSpiller1 = finnSpillerInfoTotalt(spiller1, "$antall spilt");
        double totalSpiltSpiller2 = finnSpillerInfoTotalt(spiller2, "$antall spilt");
        double totalVunnetSpiller1 = finnSpillerInfoTotalt(spiller1, "$vunnet");
        double totalVunnetSpiller2 = finnSpillerInfoTotalt(spiller2, "$vunnet");

        double sannsSpiller1 = finnSannsSpiller(spilt, vunnetSpiller1, forrigeSpiltSpiller1, forrigeVunnetSpiller1, totalSpiltSpiller1, totalVunnetSpiller1);
        double sannsSpiller2 = finnSannsSpiller(spilt, vunnetSpiller2, forrigeSpiltSpiller2, forrigeVunnetSpiller2, totalSpiltSpiller2, totalVunnetSpiller2);

        return (sannsSpiller1/(sannsSpiller1+sannsSpiller2));
    }

    // Hjelpemetode for å få ut sannsynlighet i prosent med to siffer etter komma
    public String formatSannsProsent(double sannsynlighet) {
        sannsynlighet *= 100;
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(sannsynlighet);
    }

    // Sannsynlighet for at en bestemt spiller vil vinne i sin gruppe
    // Kjøres før hver sesong, vil ikke gi fornuftige resultater midt i en sesong
    public double sannsSpillerVilVinneIGruppe(String spiller, int sesong) throws Exception {
        // Finner gjennomsnittlig sannsynlighet for å vinne mot motspillerene i sin gruppe
        double personSannsStat = 0.0;
        DBObject personQuery = new BasicDBObject();
        personQuery.put("person_navn", spiller);
        personQuery.put("sesong", sesong);
        DBObject person = dbHandterer.hentResultatColl().findOne(personQuery);
        int gruppe = Integer.parseInt(person.get("gruppe").toString());
        int divisjon = Integer.parseInt(person.get("divisjon").toString());
        DBObject gruppeQuery = new BasicDBObject();
        gruppeQuery.put("sesong", sesong);
        gruppeQuery.put("divisjon", divisjon);
        gruppeQuery.put("gruppe", gruppe);
        DBCursor cursor = dbHandterer.hentResultatColl().find(gruppeQuery);
        double gruppestr = cursor.size();
        while (cursor.hasNext()) {
            DBObject gruppeMedlem = cursor.next();
            String motspiller = gruppeMedlem.get("person_navn").toString();
            //Vil ikke inkludere spillerens sannsynlighet for å vinne mot seg selv
            if (!motspiller.equalsIgnoreCase(spiller)) {
                personSannsStat += sannsSpillerVilVinne(spiller, motspiller, sesong);
            }
        }
        personSannsStat /= gruppestr;
        // Finner spillerens gjennomsnittlige plassering
        double gjennomsnittPlassering = finnSpillerInfoTotalt(spiller, "$plassering");
        DBCursor plassResTab = dbHandterer.hentResultatColl().find(new BasicDBObject("person_navn", spiller));
        double resStr = plassResTab.size();
        gjennomsnittPlassering /= resStr;
        // Finner gjennomsnittlig gruppestørrelse
        double antMuligePlasseringer = finnSpillerInfoTotalt(spiller, "$antall spillere");
        antMuligePlasseringer /= resStr;
        // Regner ut gunstige over mulige utfall for plassering for videre bruk i sannsynlighetsformel
        double plassStat = (antMuligePlasseringer - gjennomsnittPlassering + 1)/antMuligePlasseringer;

        // Regner ut en faktor basert på tidligere plasseringer og vinnersjanser mot motspillerene
        double faktor = (plassStat + personSannsStat)/2 + 0.5;

        // Regner ut endelig sannsynlighet
        return (1/gruppestr)*faktor;
    }

    // Henter alle spillere sortert på rank
    /*public DBCursor hentAlleEtterRank() throws Exception {
        DBCursor cursor = dbHandterer.hentPersonColl().find();
        cursor.sort(new BasicDBObject("Rank", 1));

        return cursor;
    }*/

    public DBCursor hentAlleEtterRank() throws Exception {
        DBObject query = new BasicDBObject("Sesong", sisteSesong());
        DBObject eleMatch = new BasicDBObject("$elemMatch", query);

        DBObject person = new BasicDBObject();
        person.put("Navn", "");
        person.put("TotaltSpilteSesonger", "");
        person.put("Ranking", eleMatch);

        DBCursor cursor = dbHandterer.hentPersonColl().find(new BasicDBObject(), person);
        cursor.sort(new BasicDBObject("Ranking", -1));

        return cursor;
    }

    // Lagrer en rank for alle spillere
   /* public void rankingAlle() throws Exception {

        DBCursor cursor = dbHandterer.hentPersonColl().find();

        ArrayList<Spiller> spillere = new ArrayList<Spiller>();
        double divisjon;
        double rank;
        String navn;

        int antall = 1;
        int total = cursor.count();
        while (cursor.hasNext()) {
            DBObject o = cursor.next();
            navn = o.get("Navn").toString();
            divisjon = hentSisteSpilteDivisjon(navn);
            rank = rankingSpiller(navn, divisjon);
            spillere.add(new Spiller(navn, rank));

            System.out.println("Ferdig med " +antall+ " av " +total+ " spillere");
            antall++;
        }

        Collections.sort(spillere);
        lagreRank(spillere);

        // Fjerner dobbeltlagrede data uten rank
        dbHandterer.hentPersonColl().remove(new BasicDBObject("Rank", 0));
    }*/

    public void nyLagreRankAlle() throws Exception {
        DBCursor cursor = dbHandterer.hentPersonColl().find();
        int teller = 1;

        while(cursor.hasNext()) {
            DBObject o = cursor.next();
            try {
                nyLagreRankSpiller(o.get("Navn").toString(), o);
            } catch(NullPointerException e) {
                System.out.println("Fant ikke spillerresultater for " +o.get("Navn").toString());
            }

            System.out.println("Ferdig med " +teller+ " av " +cursor.size());
            teller++;
        }
    }

    public void nyLagreRankSpiller(String navn, DBObject o) throws Exception {
        DBCursor cursor = dbHandterer.hentResultatColl().find(new BasicDBObject("person_navn", navn));
        BasicDBList liste = new BasicDBList();
        double forrigeRank = 0;
        int forrigeDivisjon = Integer.parseInt(cursor.one().get("divisjon").toString());
        int sesongTeller = Integer.parseInt(cursor.one().get("sesong").toString());

        while(cursor.hasNext()) {
            DBObject spiller = cursor.next();

            int sesong = Integer.parseInt(spiller.get("sesong").toString());

            // Fyller mellomrom hvis en spiller ikke har spilt noen sesonger
            while (sesongTeller < sesong) {
                double rankSesong = 5000 - (forrigeDivisjon*900);
                double rank = (rankSesong+forrigeRank) / 2;
                DBObject rankObjekt = new BasicDBObject("Sesong", sesongTeller).append("Rank", rank);
                liste.add(rankObjekt);
                forrigeRank = rank;
                sesongTeller++;
            }

            double antSpilt = Double.parseDouble(spiller.get("antall spilt").toString());
            double rank;
            if (antSpilt != 0) {
                double divisjonstall = Double.parseDouble(spiller.get("divisjon").toString()) * 900;
                double poengTall = ((Double.parseDouble(spiller.get("kamppoeng").toString()) / antSpilt) * 850);
                double marginTall = (((Double.parseDouble(spiller.get("poeng for").toString()) - Double.parseDouble(spiller.get("poeng mot").toString())) / antSpilt) * 4);

                rank = 5000 - divisjonstall + poengTall + marginTall;
            } else {
                rank = 5000 - (forrigeDivisjon*900);
            }

            if (forrigeRank == 0) {
                DBObject rankObjekt = new BasicDBObject("Sesong", sesong).append("Rank", rank);
                liste.add(rankObjekt);
                forrigeRank = rank;
            } else {
                double nyRank = (rank+forrigeRank) / 2;
                DBObject rankObjekt = new BasicDBObject("Sesong", sesong).append("Rank", nyRank);
                liste.add(rankObjekt);
                forrigeRank = nyRank;
            }

            forrigeDivisjon = Integer.parseInt(spiller.get("divisjon").toString());
            sesongTeller++;
        }

        // Fyller inn rank opp til nåværende sesong dersom en spiller ikke har deltatt
        int sisteSesong = sisteSesong();
        while(sesongTeller <= sisteSesong) {
            double rankSesong = 5000 - (forrigeDivisjon*900);
            double rank = (rankSesong+forrigeRank) / 2;
            DBObject rankObjekt = new BasicDBObject("Sesong", sesongTeller).append("Rank", rank);
            liste.add(rankObjekt);
            forrigeRank = rank;
            sesongTeller++;
        }

        DBObject nySpiller = new BasicDBObject("_id", o.get("_id"))
                .append("Navn", o.get("Navn"))
                .append("Sted", o.get("Sted"))
                .append("Født", o.get("Født"))
                .append("TotaltSpilteSesonger", cursor.size())
                .append("Ranking", liste);

        dbHandterer.hentTestColl().insert(nySpiller);
    }

    // HJELPEMETODER

    // Hjelpemetode til rankingAlle(). Regner ut en rank for en spiller
    private double rankingSpiller(String spiller, double divisjon) throws Exception {
        double totalKamppoeng = finnSpillerInfoTotalt(spiller, "$kamppoeng");
        double totalSpilt = finnSpillerInfoTotalt(spiller, "$antall spilt");
        double totalWFPoengFor = finnSpillerInfoTotalt(spiller, "$poeng for");
        double totalWFPoengMot = finnSpillerInfoTotalt(spiller, "$poeng mot");
        double totalWFPoeng = totalWFPoengFor+totalWFPoengMot;

        // Hvis personen ikke har spilt noen kamper, eller ikke har fått registrert noen wordfeudpoeng hverken for eller mot seg,
        // får han -1 i rank
        if (totalSpilt == 0 || totalWFPoeng == 0) {
            return -1;
        }

        double teller = (totalKamppoeng/(totalSpilt*2)) + (totalWFPoengFor/totalWFPoeng) *0.5;
        double nevner = Math.pow(2, divisjon);

        return teller/nevner;
    }

    // Hjelpemetode til rankingAlle(). Lagrer spillerens rank i databasen.
    private void lagreRank(ArrayList<Spiller> spillere) {

        for (int i = 0; i < spillere.size(); i++) {
            DBCursor cursor = dbHandterer.hentPersonColl().find(new BasicDBObject("Navn", spillere.get(i).getNavn()));
            DBObject o = cursor.next();

            DBObject person = new BasicDBObject("_id", o.get("_id"));
            person.put("Navn", o.get("Navn"));
            person.put("Sted", o.get("Sted"));
            person.put("Født", o.get("Født"));
            person.put("Rank", i+1);
            dbHandterer.hentPersonColl().update(new BasicDBObject("_id", o.get("_id")), person);
        }
    }

    // Lagrer
    public void lagreTotaltSpilteSesonger() throws Exception {
        DBCursor cursor = dbHandterer.hentPersonColl().find();
        int teller = 1;
        while (cursor.hasNext()) {
            DBObject o = cursor.next();
            DBObject person = new BasicDBObject("_id", o.get("_id"));
            String navn = o.get("Navn").toString();
            person.put("Navn", navn);
            person.put("Sted", o.get("Sted"));
            person.put("Født", o.get("Født"));
            person.put("Rank", o.get("Rank"));
            person.put("AntallSpilteSesonger", hentTotaltSpilteSesonger(navn));
            dbHandterer.oppdaterPerson(person);
            System.out.println("Oppdatert " +teller+ " av " +cursor.count()+ " personer");
            teller++;
        }
    }

    // Hjelpemetode til lagreTotaltSpilteSesonger(). Finner hvor mange sesonger en spiller har deltatt i.
    private int hentTotaltSpilteSesonger(String navn) {
        DBCursor cursor = dbHandterer.hentResultatColl().find(new BasicDBObject("person_navn", navn));
        return cursor.count();
    }

    // Henter ut en spillers sist spilte divisjon
    private int hentSisteSpilteDivisjon(String spiller) throws Exception {

        BasicDBObject match = new BasicDBObject(
                "$match", new BasicDBObject("person_navn", spiller)
        );

        BasicDBObject group = new BasicDBObject(
                "$group", new BasicDBObject("_id", null).append(
                "siste", new BasicDBObject("$last", "$divisjon")
        )
        );

        AggregationOutput output = dbHandterer.hentResultatColl().aggregate(match, group);

        int siste = 0;
        for (DBObject i : output.results()) {
            siste = (Integer)i.get("siste");
        }
        return siste;
    }

    // Hjelpemetode til sannsSpillerVilVinne(). Finner brøkuttrykk for en spiller.
    private double finnSannsSpiller(double spilt, double vunnet, double forrigeSpilt, double forrigeVunnet, double totaltSpilt, double totaltVunnet) {
        double brøk1;
        if (spilt == 0) {
            brøk1 = 0;
        } else {
            brøk1 = vunnet/spilt;
        }

        double brøk2;
        if (forrigeSpilt == 0) {
            brøk2 = 0;
        } else {
            brøk2 = forrigeVunnet/forrigeSpilt;
        }

        double brøk3;
        if (totaltSpilt == 0) {
            brøk3 = 0;
        } else {
            brøk3 = totaltVunnet/totaltSpilt;
        }

        return brøk1+brøk2+brøk3;
    }

    // Finner summen av Wordfeudpoeng i en sesong
    public int finnSesongInfo(int sesong, String kategori) throws Exception {

        BasicDBObject match = new BasicDBObject(
                "$match", new BasicDBObject("sesong", sesong)
        );

        BasicDBObject group = new BasicDBObject(
                "$group", new BasicDBObject("_id", null).append(
                "total", new BasicDBObject("$sum", kategori)
        )
        );

        AggregationOutput output = dbHandterer.hentResultatColl().aggregate(match, group);

        int total = 0;
        for (DBObject i : output.results()) {
            total = (Integer)i.get("total");
        }
        return total;
    }

    // Finner summen av Wordfeudpoeng i en divisjon
    public int finnDivisjonInfo(int sesong, int divisjon, String kategori) throws Exception {

        BasicDBObject match = new BasicDBObject(
                "$match", new BasicDBObject("sesong", sesong).append("divisjon", divisjon)
        );

        BasicDBObject group = new BasicDBObject(
                "$group", new BasicDBObject("_id", null).append(
                "total", new BasicDBObject("$sum", kategori)
        )
        );

        AggregationOutput output = dbHandterer.hentResultatColl().aggregate(match, group);

        int total = 0;
        for (DBObject i : output.results()) {
            total = (Integer)i.get("total");
        }
        return total;
    }

    // Finner summen av Wordfeudpoeng i en gruppe
    public int finnGruppeInfo(int sesong, int divisjon, int gruppe, String kategori) throws Exception {

        BasicDBObject match = new BasicDBObject(
                "$match", new BasicDBObject("sesong", sesong).append("divisjon", divisjon).append("gruppe", gruppe)
        );

        BasicDBObject group = new BasicDBObject(
                "$group", new BasicDBObject("_id", null).append(
                "total", new BasicDBObject("$sum", kategori)
        )
        );

        AggregationOutput output = dbHandterer.hentResultatColl().aggregate(match, group);

        int total = 0;
        for (DBObject i : output.results()) {
            total = (Integer)i.get("total");
        }
        return total;
    }

    // Hjelpemetode til sannsSpillerVilVinne(). Finner summen av totalt antall spilt eller totalt antall vunnet
    // for en spiller, bestemt av kategorien.
    private int finnSpillerInfoTotalt(String spiller, String kategori) throws Exception {
        BasicDBObject match = new BasicDBObject(
                "$match", new BasicDBObject("person_navn", spiller)
        );

        BasicDBObject group = new BasicDBObject(
                "$group", new BasicDBObject("_id", null).append(
                "total", new BasicDBObject("$sum", kategori)
        )
        );

        AggregationOutput output = dbHandterer.hentResultatColl().aggregate(match, group);

        int total = 0;
        for (DBObject i : output.results()) {
            total = (Integer)i.get("total");
        }
        return total;
    }

    // Hjelpemetode til sannsSpillerVilVinne(). Finner summen av antall kamper spilt forrige sesong
    // eller antall kamper vunnet forrige sesong for en spiller, bestemt av kategorien.
    private int finnSpillerInfoForrigeSesong(String spiller, String kategori, int nåværendeSesong) throws Exception {
        BasicDBObject match = new BasicDBObject(
                "$match", new BasicDBObject("person_navn", spiller).append("sesong", nåværendeSesong-1)
        );

        BasicDBObject group = new BasicDBObject(
                "$group", new BasicDBObject("_id", null).append(
                "total", new BasicDBObject("$sum", kategori)
        )
        );

        AggregationOutput output = dbHandterer.hentResultatColl().aggregate(match, group);

        int total = 0;
        for (DBObject i : output.results()) {
            total = (Integer)i.get("total");
        }
        return total;
    }

    public JsonArray spillerResultater(String spiller) throws Exception {
        JsonArray liste = new JsonArray();
        DBObject query = new BasicDBObject();
        query.put("person_navn", spiller);
        DBCursor cursor = dbHandterer.hentResultatColl().find(query);
        while(cursor.hasNext()) {
            DBObject o = cursor.next();
            JsonObject res = new JsonObject();
            res.addProperty("Sesong", Integer.parseInt(o.get("sesong").toString()));
            res.addProperty("Vunnet", Integer.parseInt(o.get("vunnet").toString()));
            res.addProperty("Uavgjort", Integer.parseInt(o.get("uavgjort").toString()));
            res.addProperty("Tapt", Integer.parseInt(o.get("tapt").toString()));
            res.addProperty("Divisjon", Integer.parseInt(o.get("divisjon").toString()));
            res.addProperty("Gruppe", Integer.parseInt(o.get("gruppe").toString()));
            res.addProperty("PoengFor", Integer.parseInt(o.get("poeng for").toString()));
            res.addProperty("PoengMot", Integer.parseInt(o.get("poeng mot").toString()));
            res.addProperty("Plassering", Integer.parseInt(o.get("plassering").toString()));
            res.addProperty("Gruppestørrelse", Integer.parseInt(o.get("antall spillere").toString()));
            liste.add(res);
        }
        return liste;
    }

    public JsonArray hentDivisjonStat(int sesong) throws Exception {
        JsonArray liste = new JsonArray();
        DBCursor cursor = dbHandterer.hentDivStatColl().find(new BasicDBObject("sesong", sesong));
        while (cursor.hasNext()) {
            DBObject o = cursor.next();
            JsonObject object = new JsonObject();
            object.addProperty("divisjon", Integer.parseInt(o.get("divisjon").toString()));
            object.addProperty("antall_grupper", Integer.parseInt(o.get("antall_grupper").toString()));
            object.addProperty("antall_spillere", Integer.parseInt(o.get("antall_spillere").toString()));
            object.addProperty("antall_wfpoeng", Integer.parseInt(o.get("antall_wfpoeng").toString()));
            object.addProperty("antall_uavgjort", Integer.parseInt(o.get("antall_uavgjort").toString()));
            object.addProperty("grupper", o.get("grupper").toString());

            /*JsonArray gruppeListe = new JsonArray();
            for(int i = 1; i <= Integer.parseInt(o.get("antall_grupper").toString()); i++) {
                JsonObject gruppeObjekt = new JsonObject();
                gruppeObjekt.addProperty("gruppe", i);
                gruppeObjekt.addProperty("antall_wfpoeng", finnGruppeInfo(sesong, Integer.parseInt(o.get("divisjon").toString()), i, "$poeng for"));
                gruppeListe.add(gruppeObjekt);
            }
            object.add("grupper", gruppeListe);*/
            liste.add(object);
        }
        return liste;
    }

    // Hjelpemetode for å sjekke om en spiller er lagret i databasen
    private boolean spillerFinnes(String spiller) {
        try{
            DBObject query = new BasicDBObject();
            query.put("Navn", spiller);
            DBCursor cursor = dbHandterer.hentPersonColl().find(query);
            cursor.one().get("Navn");
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    // Hjelpemetode for å finne siste spilte sesong
    public int sisteSesong() {
        DBObject res = dbHandterer.hentResultatColl().find().sort(new BasicDBObject("sesong", -1)).limit(1).one();
        String resTekst = res.get("sesong").toString();
        int sesong = 0;
        try {
            sesong = Integer.parseInt(resTekst);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return -1;
        }
        return sesong;
    }

    public int sisteDivisjon(int sesong) {
        DBObject query = new BasicDBObject("sesong", sesong);
        DBObject res = dbHandterer.hentResultatColl().find(query).sort(new BasicDBObject("divisjon", -1)).limit(1).one();
        String resTekst = res.get("divisjon").toString();
        int divisjon = 0;
        try {
            divisjon = Integer.parseInt(resTekst);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return -1;
        }
        return divisjon;
    }

    public int sisteGruppe(int sesong, int divisjon) {
        DBObject query = new BasicDBObject("sesong", sesong);
        query.put("divisjon", divisjon);
        DBObject res = dbHandterer.hentResultatColl().find(query).sort(new BasicDBObject("gruppe", -1)).limit(1).one();
        String resTekst = res.get("gruppe").toString();
        int gruppe = 0;
        try {
            gruppe = Integer.parseInt(resTekst);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return -1;
        }
        return gruppe;
    }
}
