import com.google.gson.JsonObject;
import com.sun.jersey.api.NotFoundException;
import com.sun.net.httpserver.HttpServer;
import com.sun.jersey.api.container.httpserver.HttpServerFactory;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;

// The Java class will be hosted at the URI path "/vinnersannsynlighet/(spiller)"
@Path("/vinnersannsynlighet/{spiller1}/{spiller2}")
public class Vinnersannsynlighet {
    // The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media type "application/json"
    @Produces({"application/json"})
    public String getClichedMessage(@PathParam("spiller1") String spiller1, @PathParam("spiller2") String spiller2) throws Exception {
        //todo: Fiks tegn-issue (æ,ø,å etc)
        Statistikk statistikk = new Statistikk();
        JsonObject objekt = new JsonObject();
        boolean suksess = false;
        if (statistikk.tilkoble()) {
            int sesong = statistikk.sisteSesong();
            if (statistikk.validerInput(spiller1, spiller2, sesong) && statistikk.spillerErISesong(spiller1, sesong) && statistikk.spillerErISesong(spiller2, sesong)) {
                double sannsynlighet = statistikk.sannsSpillerVilVinne(spiller1, spiller2, sesong);
                String sannsRes;
                sannsRes = statistikk.formatSannsProsent(sannsynlighet);
                objekt.addProperty("Spiller1", spiller1);
                objekt.addProperty("Spiller2", spiller2);
                objekt.addProperty("Sannsynlighet", sannsRes);
                objekt.addProperty("Sesong", sesong);
                suksess = true;
            }
            statistikk.frakoble();
        }
        if (!suksess) {
            throw new NotFoundException();
        }
        return objekt.toString();
    }

    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServerFactory.create("http://localhost:9995/");
        server.start();

        System.out.println("Server running");
        System.out.println("Visit: http://localhost:9991/vinnersannsynlighet/spiller1/spiller2/sesong");
        System.out.println("Hit return to stop...");
        System.in.read();
        System.out.println("Stopping server");
        server.stop(0);
        System.out.println("Server stopped");
    }
}
