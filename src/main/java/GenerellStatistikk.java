import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.sun.jersey.api.NotFoundException;
import com.sun.net.httpserver.HttpServer;
import com.sun.jersey.api.container.httpserver.HttpServerFactory;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;

// The Java class will be hosted at the URI path "/ranking"
@Path("/statistikk")
public class GenerellStatistikk {
    // The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media type "application/json"
    @Produces({"application/json"})
    public String getClichedMessage() throws Exception {

        Statistikk statistikk = new Statistikk();
        JsonArray stat = new JsonArray();
        boolean suksess = false;

        if(statistikk.tilkoble()) {
            DBCursor cursor = statistikk.getDbHandterer().hentStatColl().find();

            while (cursor.hasNext()) {
                DBObject o = cursor.next();
                JsonObject object = new JsonObject();
                int sesong = Integer.parseInt(o.get("sesong").toString());
                object.addProperty("sesong", sesong);
                object.addProperty("antall_divisjoner", o.get("antall_divisjoner").toString());
                object.addProperty("antall_grupper", o.get("antall_grupper").toString());
                object.addProperty("antall_spillere", o.get("antall_spillere").toString());
                object.addProperty("antall_uavgjort", o.get("antall_uavgjort").toString());
                object.addProperty("antall_wfpoeng", o.get("antall_wfpoeng").toString());

                JsonArray list = statistikk.hentDivisjonStat(sesong);
                object.add("divisjoner", list);
                stat.add(object);
            }
            statistikk.frakoble();
            suksess = true;
        }

        if (!suksess) {
            throw new NotFoundException();
        }
        return String.valueOf(stat);
    }

    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServerFactory.create("http://localhost:9995/");
        server.start();

        System.out.println("Server running");
        System.out.println("Visit: http://localhost:9995/statistikk");
        System.out.println("Hit return to stop...");
        System.in.read();
        System.out.println("Stopping server");
        server.stop(0);
        System.out.println("Server stopped");
    }
}
