public class Spiller implements Comparable<Spiller> {

    private String navn;
    private double rank;

    public Spiller(String navn, double rank) {
        this.navn = navn;
        this.rank = rank;
    }

    public String getNavn() {
        return navn;
    }

    public double getRank() {
        return rank;
    }

    public int compareTo(Spiller o) {
        return new Double((o.getRank())).compareTo(this.getRank());
    }

    @Override
    public String toString() {
        return navn +": "+ rank;
    }
}
