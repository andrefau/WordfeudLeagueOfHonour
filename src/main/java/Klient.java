import com.mongodb.*;

public class Klient {

    public static void opprettDatabase(int sisteSesong) {
        JsonLeser leser = new JsonLeser();
        Statistikk s = new Statistikk();

        try {
            System.out.println("----Henter og lagrer resultater og spillerinfo----");
            leser.tilkoble();
            leser.hentResultater(1, sisteSesong);

            System.out.println("\n----Henter og lagrer kampinfo----");
            leser.hentKamper(sisteSesong);
            leser.frakoble();

            /*System.out.println("\n----Regner ut og lagrer en rank for alle spillerene----");
            s.tilkoble();
            s.rankingAlle();
            s.frakoble();*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws Exception {

        // Angi opp til hvilken sesong du vil lagre data
        //Klient.opprettDatabase(10);
        Statistikk statistikk = new Statistikk();
        statistikk.tilkoble();

        statistikk.lagreDivisjonStatistikk();

        statistikk.frakoble();
    }
}
