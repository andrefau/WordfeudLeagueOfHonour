import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.sun.jersey.api.NotFoundException;
import com.sun.net.httpserver.HttpServer;
import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import org.codehaus.jettison.json.JSONObject;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;

// The Java class will be hosted at the URI path "/spillere/(navn)"
@Path("/spillere/{navn}")
public class SpillerInfo {
    // The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media type "application/json"
    @Produces({"application/json"})
    public String getClichedMessage(@PathParam("navn") String spiller) throws Exception {
        //todo: Fiks tegn-issue (æ,ø,å etc)
        Statistikk statistikk = new Statistikk();
        JsonObject objekt = new JsonObject();
        boolean suksess = false;

        if(statistikk.tilkoble()) {
            if (statistikk.validerInput(spiller)) {
                DBObject o = statistikk.finnSpillerInfo(spiller);
                objekt.addProperty("Navn", spiller);
                objekt.addProperty("Url", o.get("_id").toString());
                objekt.addProperty("Sted", o.get("Sted").toString());
                objekt.addProperty("Fodt", o.get("Født").toString());
                objekt.addProperty("Ranking", o.get("Ranking").toString());

                JsonArray list = statistikk.spillerResultater(spiller);
                objekt.add("Resultater", list);
                suksess = true;
            }
            statistikk.frakoble();
        }

        if (!suksess) {
            throw new NotFoundException();
        }
        return objekt.toString();
    }

    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServerFactory.create("http://localhost:9995/");
        server.start();

        System.out.println("Server running");
        System.out.println("Visit: http://localhost:9995/spillere/navn");
        System.out.println("Hit return to stop...");
        System.in.read();
        System.out.println("Stopping server");
        server.stop(0);
        System.out.println("Server stopped");
    }
}
