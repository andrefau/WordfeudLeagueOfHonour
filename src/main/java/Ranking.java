import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.sun.jersey.api.NotFoundException;
import com.sun.net.httpserver.HttpServer;
import com.sun.jersey.api.container.httpserver.HttpServerFactory;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;

// The Java class will be hosted at the URI path "/ranking"
@Path("/ranking")
public class Ranking {
    // The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media type "application/json"
    @Produces({"application/json"})
    public String getClichedMessage() throws Exception {
        //todo: Fiks tegn-issue (æ,ø,å etc)
        Statistikk statistikk = new Statistikk();
        JsonArray spillere = new JsonArray();
        boolean suksess = false;

        if(statistikk.tilkoble()) {
            DBCursor cursor = statistikk.hentAlleEtterRank();
            int teller = 1;
            while (cursor.hasNext()) {
                DBObject o = cursor.next();
                JsonObject object = new JsonObject();

                /*object.addProperty("Rank", o.get("Rank").toString());
                object.addProperty("Navn", o.get("Navn").toString());
                object.addProperty("AntallSpilteSesonger", o.get("AntallSpilteSesonger").toString());*/

                object.addProperty("Navn", o.get("Navn").toString());
                object.addProperty("TotaltSpilteSesonger", o.get("TotaltSpilteSesonger").toString());
                object.addProperty("Rank", teller);
                DBObject liste = (BasicDBList)o.get("Ranking");
                DBObject rating = (BasicDBObject)liste.get("0");
                object.addProperty("Rating", rating.get("Rank").toString());

                spillere.add(object);
                teller++;
            }
            statistikk.frakoble();
            suksess = true;
        }

        if (!suksess) {
            throw new NotFoundException();
        }
        return String.valueOf(spillere);
    }

    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServerFactory.create("http://localhost:9995/");
        server.start();

        System.out.println("Server running");
        System.out.println("Visit: http://localhost:9995/ranking");
        System.out.println("Hit return to stop...");
        System.in.read();
        System.out.println("Stopping server");
        server.stop(0);
        System.out.println("Server stopped");
    }
}