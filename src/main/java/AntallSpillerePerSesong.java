import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.jersey.api.NotFoundException;
import com.sun.net.httpserver.HttpServer;
import com.sun.jersey.api.container.httpserver.HttpServerFactory;

import java.io.IOException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;

// The Java class will be hosted at the URI path "/antallspillerepersesong"
@Path("/antallspillerepersesong")
public class AntallSpillerePerSesong {
    // The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media type "application/json"
    @Produces({"application/json"})
    public String getClichedMessage() throws Exception {
        //todo: Fiks tegn-issue (æ,ø,å etc)
        Statistikk statistikk = new Statistikk();
        JsonArray antallSpillere = new JsonArray();
        boolean suksess = false;

        if(statistikk.tilkoble()) {
            ArrayList<Integer> res = statistikk.finnAntallSpillerePerSesong();
            for(int i = 0; i < res.size(); i++) {
                JsonObject objekt = new JsonObject();
                objekt.addProperty("Sesong", i+1);
                objekt.addProperty("Antall", res.get(i));
                antallSpillere.add(objekt);
            }
            statistikk.frakoble();
            suksess = true;
        }
        if (!suksess) {
            throw new NotFoundException();
        }
        return String.valueOf(antallSpillere);
    }

    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServerFactory.create("http://localhost:9995/");
        server.start();

        System.out.println("Server running");
        System.out.println("Visit: http://localhost:9995/antallspillerepersesong");
        System.out.println("Hit return to stop...");
        System.in.read();
        System.out.println("Stopping server");
        server.stop(0);
        System.out.println("Server stopped");
    }
}