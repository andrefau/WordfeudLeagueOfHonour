import static org.junit.Assert.assertNotNull;

import com.mongodb.BasicDBObject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class DatabaseHandtererTest {

    private static DatabaseHandterer tester;

    @BeforeClass
    public static void testSetup() throws Exception {
        tester = new DatabaseHandterer();
        tester.tilkoble();
    }

    @AfterClass
    public static void testCleanup() {
        tester.hentResultatColl().remove(new BasicDBObject("person_navn", "TestNavn"));
        tester.hentPersonColl().remove(new BasicDBObject("Navn", "TestNavn"));
        tester.hentKampColl().remove(new BasicDBObject("sesong", 999));
        tester.frakoble();
    }

    @Test
    public void testLagreResultat() throws Exception {
        tester.lagreResultat(999, 999, 999, 999, 999, 999, 999, 999, 999, 999, 999, 999, "TestNavn");
        assertNotNull(tester.hentResultatColl().find(new BasicDBObject("person_navn", "TestNavn")));
    }

    @Test
    public void testLagrePersoner() throws Exception {
        tester.lagrePersoner("TestNavn", "TestUrl", "TestSted", "TestFødt", 999);
        assertNotNull(tester.hentPersonColl().find(new BasicDBObject("Navn", "TestNavn")));
    }

    @Test
    public void testLagreKamp() throws Exception {
        tester.lagreKamp(999, "TestNavn1", "TestNavn2", 999, 999);
        assertNotNull(tester.hentKampColl().find(new BasicDBObject("sesong", 999)));
    }
}
