import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;

public class StatistikkTest {

    private static Statistikk tester;

    @BeforeClass
    public static void testSetup() throws Exception{
        tester = new Statistikk();
        tester.tilkoble();
    }

    @AfterClass
    public static void testCleanup() {
        tester.frakoble();
    }

    @Test
    public void testFinnAntallSpillereSesong() {
        assertEquals("Result", 70, tester.finnAntallSpillereSesong(8));
        assertEquals("Result", 143, tester.finnAntallSpillereSesong(10));
        assertEquals("Result", 0, tester.finnAntallSpillereSesong(9872));
        assertEquals("Result", 0, tester.finnAntallSpillereSesong(-5));
    }

    @Test
    public void testFinnAntallSpillerPerSesong() {
        ArrayList<Integer> liste = tester.finnAntallSpillerePerSesong();
        int test1 = liste.get(0);
        int test2 = liste.get(1);
        int test3 = liste.get(9);
        assertEquals("Result", 4, test1);
        assertEquals("Result", 5, test2);
        assertEquals("Result", 143, test3);
    }

    @Test
    public void testValiderInput() {
        assertEquals("Result", false, tester.validerInput("UgyldigTestNavn"));
        assertEquals("Result", false, tester.validerInput("luske"));
        assertEquals("Result", true, tester.validerInput("Luske"));
    }

    @Test
    public void testValiderInput2() {
        assertEquals("Result", false, tester.validerInput("sigvei", 9780));
        assertEquals("Result", false, tester.validerInput("Luske", -4));
        assertEquals("Result", false, tester.validerInput("UgyldigTestNavn", 4));
        assertEquals("Result", true, tester.validerInput("sigvei", 8));
        assertEquals("Result", true, tester.validerInput("Luske", 5));
    }

    @Test
    public void testValiderInput3() {
        assertEquals("Result", false, tester.validerInput("Luske", "UgyldigTestNavn", 5));
        assertEquals("Result", false, tester.validerInput("sigvei", "Luske", 6907));
        assertEquals("Result", false, tester.validerInput("sigvei", "Luske", 0));
        assertEquals("Result", true, tester.validerInput("KirstiPirsti", "MrHue", 5));
        assertEquals("Result", true, tester.validerInput("Kniven", "eterem", 10));
    }

    @Test
    public void testSannsSpillerVilVinne() throws Exception{
        assertEquals("Result", "42,42", tester.formatSannsProsent(tester.sannsSpillerVilVinne("Kniven", "eterem", 10)));
        assertEquals("Result", "62,01", tester.formatSannsProsent(tester.sannsSpillerVilVinne("sigvei", "Longlazy", 8)));
        assertEquals("Result", "100", tester.formatSannsProsent(tester.sannsSpillerVilVinne("Lynrask", "UgyldigTestNavn", 3)));
    }

    @Test
    public void testSannsSpillerVilVinneIGruppe() throws Exception {
        assertEquals("Result", "9,69", tester.formatSannsProsent(tester.sannsSpillerVilVinneIGruppe("Kniven", 10)));
        assertEquals("Result", "11,18", tester.formatSannsProsent(tester.sannsSpillerVilVinneIGruppe("sigvei", 8)));

    }

    @Test(expected = NullPointerException.class)
    public void testSannsSpillerVilVinneIGruppeException() throws Exception{
        tester.sannsSpillerVilVinneIGruppe("eterem", 20);
        tester.sannsSpillerVilVinneIGruppe("UgyldigTestNavn", 12);
    }

}
