import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

public class SpillerInfoTest {

    private static SpillerInfo tester;

    @BeforeClass
    public static void testSetup() {
        tester = new SpillerInfo();
    }

    @Test
    public void testGetBeskjed() throws Exception {
        assertEquals("Result", "{\"Navn\":\"Kniven\",\"Url\":\"http://leagues.aasmul.net/bruker-30\",\"Sted\":\"\",\"Født\":\"\",\"Rank\":\"24\"}", tester.getClichedMessage("Kniven"));
        assertEquals("Result", "{\"Navn\":\"eterem\",\"Url\":\"http://leagues.aasmul.net/bruker-22\",\"Sted\":\"\",\"Født\":\"\",\"Rank\":\"25\"}", tester.getClichedMessage("eterem"));
        assertEquals("Result", "{\"Navn\":\"\",\"Url\":\"\",\"Sted\":\"\",\"Født\":\"\",\"Rank\":\"\"}", tester.getClichedMessage("UgyldigTestNavn"));
    }
}
