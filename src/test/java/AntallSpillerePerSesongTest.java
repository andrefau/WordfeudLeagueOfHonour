import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

public class AntallSpillerePerSesongTest {

    private static AntallSpillerePerSesong tester;

    @BeforeClass
    public static void testSetup() {
        tester = new AntallSpillerePerSesong();
    }

    @Test
    public void testGetBeskjed() throws Exception {
        assertEquals("Result", "[{\"Sesong\":1,\"Antall spillere\":4},{\"Sesong\":2,\"Antall spillere\":5},{\"Sesong\":3,\"Antall spillere\":13},{\"Sesong\":4,\"Antall spillere\":19},{\"Sesong\":5,\"Antall spillere\":27},{\"Sesong\":6,\"Antall spillere\":30},{\"Sesong\":7,\"Antall spillere\":56},{\"Sesong\":8,\"Antall spillere\":70},{\"Sesong\":9,\"Antall spillere\":85},{\"Sesong\":10,\"Antall spillere\":143}]", tester.getClichedMessage());
    }
}
