import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
    public static void main(String[] args) {

        Result result = JUnitCore.runClasses(DatabaseHandtererTest.class, StatistikkTest.class, RankingTest.class, AntallSpillerePerSesongTest.class, SannsynlighetGruppeseierTest.class, VinnersannsynlighetTest.class, SpillerInfoTest.class);

        for (Failure f : result.getFailures()){
            System.out.println(f.toString());
        }

        if (result.wasSuccessful()) {
            System.out.println("Alle tester kjørte uten feil.");
        }
    }
}
