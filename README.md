# WordfeudLeagueOfHonour
Skoleprosjekt: back-end database for ranking til Wordfeud League of Honour. Skal stå ferdig januar 2016.

### MongoDB

Kommandoer             |Beskrivelse
--------------------------------|---------------------------------------------------------
mongo                           | starter shellprogrammet for MongoDB
db                              | viser nåværende database
show dbs                        | viser alle databaser
use *navn*                      | bytter database (oppretter en ny hvis den ikke finnes)
show collections                | viser alle collections i databasen
db.*collection*.find()          | lister ut en collection
db.*collection*.find().pretty() | lister ut en collection på standard JSON format
db.*collection*.drop()          | sletter en collection
exit                            | avslutter shellprogrammet for MongoDB
help                            | oversikt over ulike kommandoer og metoder
